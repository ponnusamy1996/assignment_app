import 'package:flutter/cupertino.dart';

class Constants {
  // Colors
  static const Color backgroundColor = Color(0xff147aa1);
  static const Color whiteColor = Color(0xffffffff);
  static const Color yellowColor = Color(0xff000000);
  static const Color accentColor = Color(0xff00415a);
  static const Color buttonColor = Color(0xff04658a);



  // Texts
  static const String appTitle = 'TIC TAC';
  static const String semiColon = ':';
  static const String newGameText = 'new game';
  static const String resetGameText = 'reset game';
  static const String resetButtonText = 'play again';
  static const String dialogMiddleText = "Reset game to play again";

  static const String versionNumber = ' ';



  // Fonts
  static const String fontFamily = '';
}
